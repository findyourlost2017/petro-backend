const axios = require("axios");

module.exports = async function (district) {
  return new Promise((resolve) => {
    axios
      .get(
        "https://shellgsllocator.geoapp.me/api/v2/locations/nearest_to?lat=22.376876&lng=114.119300&limit=50&format=json"
      )
      .then((response) => {
        resolve(response.data.locations);
      })
      .catch((error) => {
        resolve(error);
        console.log(error);
      });
  });
};
