const axios = require("axios");

module.exports = async function (district) {
  return new Promise((resolve) => {
    axios
      .post(
        "https://m.sinopechk.com/mss/stnInfo/mss-getNearStnInfo",
        {
          longitude: 114.2079796006944,
          latitude: 22.3825230577257,
          range: 100000,
          products: [],
          serviceIds: [],
          stnname: "",
        },
        {
          headers: {
            "app-version": 1530,
            platform: "02",
            Cookie:
              "acw_tc=0bc1599816665040947461341e60b74e06c94b2dcf767d0b347d4219ba44a8;",
          },
        }
      )
      .then((response) => {
        resolve(response.data.result);
      })
      .catch((error) => {
        resolve(error);
        console.log(error);
      });
  });
};
