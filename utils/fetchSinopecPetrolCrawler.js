const Crawler = require("crawler");

module.exports = async function (district) {
  const c = new Crawler({
    maxConnections: 10,
    callback: function (error, res, done) {
      if (error) {
        console.log(error);
      } else {
        const $ = res.$;

        $(".gasStationlist li .box") &&
          $(".gasStationlist li .box").each((index, ele) => {
            const item = {};
            item["short_name"] = $(ele).find("b a").text().trim();
            item["address"] = $(ele)
              .find(".fl p:first-child")
              ?.text()
              ?.trim()
              .replace("地址:", "");
            item["tel"] = $(ele)
              .find(".fl p:nth-child(2)")
              ?.text()
              ?.trim()
              .replace("電話:", "");
            let services = $(ele)
              .find(".fr p:nth-child(2)")
              ?.text()
              ?.trim()
              .replace("服務:", "");

            if (services) {
              services = services.split(",");
            }

            item["inflator"] = true;
            item["wash"] = services && services.indexOf("電腦洗車") > -1;
            item["engine_oil"] = services && services.indexOf("換油服務") > -1;
            item["convenient_store"] =
              services && services.indexOf("EASYJOY便利店") > -1;

            console.log(item);
          });
      }
      done();
    },
  });

  return new Promise((resolve) => {
    c.queue("http://www.sinopechk.com/gasstation.aspx?ser=");
  });
};
