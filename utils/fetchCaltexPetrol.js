const axios = require("axios");

module.exports = async function (district) {
  return new Promise((resolve) => {
    axios
      .get(
        "https://www.caltex.com/bin/services/getStations.json?pagePath=/content/caltex/hk/zh/find-a-caltex-station&siteType=b2c"
      )
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        resolve(error);
        console.log(error);
      });
  });
};
