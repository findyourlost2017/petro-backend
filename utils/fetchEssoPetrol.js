const axios = require("axios");

module.exports = async function (district) {
  return new Promise((resolve) => {
    axios
      .get(
        "https://www.esso.com.hk/zh-HK/api/RetailLocator/GetRetailLocations?Latitude1=22.253456355359816&Latitude2=22.412241690390804&Longitude1=113.96222907816335&Longitude2=114.37524635111257&DataSource=RetailGasStations&Country=HK&ResultLimit=250"
      )
      .then((response) => {
        resolve(response.data.Locations);
      })
      .catch((error) => {
        resolve(error);
        console.log(error);
      });
  });
};
