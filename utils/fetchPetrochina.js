const axios = require("axios");

module.exports = async function (district) {
  return new Promise((resolve) => {
    axios
      .get(
        "https://www.petrochinaintl.com.hk/api/index.php/v1/station/list?lang=zh-TW"
      )
      .then((response) => {
        resolve(response.data.data);
      })
      .catch((error) => {
        resolve(error);
        console.log(error);
      });
  });
};
