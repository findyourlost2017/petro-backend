var express = require("express");
var router = express.Router();
const _ = require("lodash");
const fetchEssoPetrol = require("../utils/fetchEssoPetrol");
const fetchShellPetrol = require("../utils/fetchShellPetrol");
const fetchCaltexPetrol = require("../utils/fetchCaltexPetrol");
const fetchSinopecPetrol = require("../utils/fetchSinopecPetrol");
const fetchPetrochina = require("../utils/fetchPetrochina");
const traditionalized = require("../utils/traditionalized");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/esso", async function (req, res, next) {
  let list = await fetchEssoPetrol();
  list = _.map(list, (item) => {
    return {
      brand: "Esso",
      address: item.AddressLine1,
      short_name: item.LocationName,
      id: item.LocationID,
      lat: item.Latitude,
      lng: item.Longitude,
      tel: item.Telephone[0],
      convenient_store: item.FilterValues.indexOf("便利店") > -1,
      inflator: item.FilterValues.indexOf("車胎充氣泵") > -1,
      wash:
        item.FilterValues.indexOf("人手洗車服務") > -1 ||
        item.FilterValues.indexOf("電腦洗車服務") > -1,
      engine_oil: item.FilterValues.indexOf("換油服務") > -1,
    };
  });
  res.json({ list });
});

router.get("/shell", async function (req, res, next) {
  let list = await fetchShellPetrol();
  list = _(list)
    .filter((item) => item.country_code === "HK")
    .map((item) => {
      return {
        brand: "Shell",
        id: item.id,
        short_name: item.name,
        address: item.address,
        lat: item.lat,
        lng: item.lng,
        tel: item.telephone,
        city: item.city,
        convenient_store: item.amenities.indexOf("selectshop") > -1,
        inflator: item.amenities.indexOf("air_and_water") > -1,
        wash: item.amenities.indexOf("carwash") > -1,
        toilet: item.amenities.indexOf("standard_toilet") > -1,
        engine_oil: item.amenities.indexOf("service_bay") > -1,
      };
    })
    .value();
  res.json({ list });
});

router.get("/caltex", async function (req, res, next) {
  let list = await fetchCaltexPetrol();
  list = _(list).map((item) => {
    return {
      brand: "Caltex",
      id: item.id,
      short_name: item.name,
      address: item.street,
      lat: item.latitude,
      lng: item.longitude,
      tel: item.phoneNumber,
      city: item.name.replace("站", ""),
      convenient_store: item.amenitiesName.indexOf("便利店 ") > -1,
      inflator: item.amenitiesName.indexOf("泵輪胎氣 ") > -1,
      wash: false,
      toilet: item.amenitiesName.indexOf("洗手間 ") > -1,
      engine_oil: item.amenitiesName.indexOf("換油服務 ") > -1,
    };
  });
  res.json({ list });
});

router.get("/sinopec", async function (req, res, next) {
  let list = await fetchSinopecPetrol();
  list = _.map(list, (item) => {
    return {
      brand: "Sinopec",
      address: item.address,
      short_name: item.stnname,
      id: item.stncode,
      lat: item.latitude,
      lng: item.longitude,
      tel: item.zipcode,
      convenient_store: _.find(item.stnServicesList, {
        stnservicename: "便利店服務",
      })
        ? true
        : false,
      inflator: true,
      wash: _.find(item.stnServicesList, {
        stnservicename: "電腦洗車服務",
      })
        ? true
        : false,
      engine_oil: _.find(item.stnServicesList, {
        stnservicename: "換油服務",
      })
        ? true
        : false,
    };
  });
  res.json({ list });
});

// https://www.petrochinaintl.com.hk/api/index.php/v1/station/list?lang=zh-TW
router.get("/petrochina", async function (req, res, next) {
  let list = await fetchPetrochina();
  list = _(list).map((item) => {
    return {
      brand: "Petro",
      id: item.station_id,
      short_name: traditionalized(item.title),
      address: traditionalized(item.address),
      lat: item.latitude,
      lng: item.longitude,
      tel: item.tel,
      city: item.title,
      convenient_store: item.service1 === "y",
      inflator: true,
      wash: item.service4 === "y",
      toilet: true,
      engine_oil: item.service2 === "y",
    };
  });
  res.json({ list });
});

module.exports = router;
